<?php

use Monorepo\Auth\Auth;
use Monorepo\Crud\Crud;
use Monorepo\App\App;

require __DIR__ . '/vendor/autoload.php';

$app = new App();
echo $app->test() . "\n";

$auth = new Auth();
echo $auth->test() . "\n";

$crud = new Crud();
echo $crud->test() . "\n";
